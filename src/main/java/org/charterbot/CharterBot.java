package org.charterbot;

import com.jagrosh.jdautilities.command.CommandClientBuilder;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import com.jagrosh.jdautilities.examples.command.AboutCommand;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Activity;
import org.charterbot.commands.*;

import javax.security.auth.login.LoginException;
import java.awt.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;


public class CharterBot
{
    public static void main( String[] args ) throws IOException, LoginException {
        List<String> list = Files.readAllLines(Paths.get("src/config.txt"));

        String token = list.get(0);
        String ownerId = list.get(1);
        String prefix = list.get(2);


        EventWaiter waiter = new EventWaiter();
        CommandClientBuilder client = new CommandClientBuilder();


        // Client ops
        client.useDefaultGame();
        client.setPrefix(prefix);
        client.setOwnerId(ownerId);
        client.setEmojis("\uD83D\uDE03", "\uD83D\uDE2E", "\uD83D\uDE26");
        client.addCommands(
                new AboutCommand(Color.CYAN, "CharterBot on Java",
                        new String[]{"More speed", "More APIs", "More memes"},
                        Permission.ADMINISTRATOR),

                            // Commands added here
                        new HelloCommand(),
                        new NemeMemesCommand(),
                        new BisonMemeCommand(),
                        new PurgeCommand(waiter)
                );

        JDABuilder.createDefault(token)


                .setStatus(OnlineStatus.DO_NOT_DISTURB)
                .setActivity(Activity.watching("the world burn"))

                .addEventListeners(waiter, client.build())

                .build();

    }
}

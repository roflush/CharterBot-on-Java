package org.charterbot.commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import net.dv8tion.jda.api.Permission;

public class PurgeCommand extends Command {


    public PurgeCommand(EventWaiter waiter) {

        this.name = "purge";
        this.help = "This removes trash from a text chat.";
        this.guildOnly = true;
        this.botPermissions = new Permission[]{Permission.ADMINISTRATOR};
    }

    @Override
    protected void execute(CommandEvent event){

        event.reply("");
    }
}

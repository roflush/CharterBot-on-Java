package org.charterbot.commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.ChannelType;

import java.awt.*;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class NemeMemesCommand extends Command {

    public NemeMemesCommand() {

        this.name = "Nemes";
        this.help = "Memes from my time in nema alliance.";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
        this.guildOnly = false;
    }

    @Override
    protected void execute(CommandEvent event) {

        List<String> givenList = Arrays.asList("https://i.imgflip.com/3kwct4.jpg",
                "https://i.imgflip.com/3kwdfh.jpg", "https://i.imgflip.com/3kwckg.jpg",
                "https://i.imgflip.com/3kwbi1.jpg", "https://i.imgflip.com/3kwc2p.jpg", "https://i.imgflip.com/3kwal0.jpg",
                "https://i.imgflip.com/3kw9kf.jpg", "https://i.imgflip.com/3kw85y.jpg", "https://i.imgflip.com/3kwlpj.jpg",
                "https://i.imgflip.com/3kwlg4.jpg", "https://i.imgflip.com/3kwkvx.jpg", "https://i.imgflip.com/3kwk88.jpg",
                "https://i.imgflip.com/3kwjwk.jpg", "https://i.imgflip.com/3kwjmz.jpg", "https://i.imgflip.com/3kwj23.jpg",
                "https://i.imgflip.com/3kwimd.jpg", "https://i.imgflip.com/3kwins.jpg", "https://i.imgflip.com/3kwi83.jpg",
                "https://i.imgflip.com/3kwgrq.jpg", "https://i.imgflip.com/3kwgh3.jpg", "https://i.imgflip.com/3kwfjf.jpg",
                "https://i.imgflip.com/3kwegm.jpg", "https://i.imgflip.com/3kwdxr.jpg", "https://i.imgflip.com/3kwdfh.jpg",
                "https://i.imgflip.com/3kx2ly.jpg", "https://i.imgflip.com/3kwnph.jpg", "https://i.imgflip.com/3kwnky.jpg",
                "https://i.imgflip.com/3kwmrw.jpg", "https://i.imgflip.com/3kwm8i.jpg", "https://i.imgflip.com/3kwm4u.jpg",
                "https://i.imgflip.com/3kwlyl.jpg");


        NemeMemesCommand obj = new NemeMemesCommand();

        event.reply(new EmbedBuilder()
                .setColor(event.isFromType(ChannelType.TEXT) ? event.getSelfMember().getColor() : Color.GREEN)
                .setFooter("Memes by Para/Roflush")
                .setTitle("Think about supporting the bot!", "")
                .setImage(obj.getRandomElement(givenList))
                .build());
    }
    public String getRandomElement(List<String> givenList){

        Random rand = new Random();
        return givenList.get(rand.nextInt(givenList.size()));
    }
}

package org.charterbot.commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.ChannelType;

import java.awt.*;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class BisonMemeCommand extends Command {

    public BisonMemeCommand() {

        this.name = "bisonmeme";
        this.help = "Memes from my time in the eve corp Bison.";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
        this.guildOnly = false;
    }

    @Override
    protected void execute(CommandEvent event) {

        List<String> givenList = Arrays.asList("https://i.imgflip.com/4bkxt9.jpg",
                "https://i.imgflip.com/4bl473.jpg", "https://i.imgflip.com/4bl4gi.jpg",
                "https://i.imgflip.com/4bl4nk.jpg", "https://i.imgflip.com/4bl4xv.jpg", "https://i.imgflip.com/4bl5e4.jpg",
                "https://i.imgflip.com/4bl5k5.jpg", "https://i.imgflip.com/4bl5vb.jpg", "https://i.imgflip.com/4bl625.jpg",
                "https://i.imgflip.com/4bnx3f.jpg", "https://i.imgflip.com/4bshon.jpg");

        BisonMemeCommand obj = new BisonMemeCommand();

        event.reply(new EmbedBuilder()
                .setColor(event.isFromType(ChannelType.TEXT) ? event.getSelfMember().getColor() : Color.GREEN)
                .setFooter("Memes by Para/Roflush")
                .setImage(obj.getRandomElement(givenList))
                .build());
    }
    public String getRandomElement(List<String> givenList){

        Random rand = new Random();
        return givenList.get(rand.nextInt(givenList.size()));
    }

}
